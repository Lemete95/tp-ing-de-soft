package ar.com.cts.sica.controller

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class Home
{
	@RequestMapping("/home")
	public ModelAndView helloWorld() {
 
		String message = "<br><div style='text-align:center;'><h3>SICA - Sistema Integrado de Cobros de Agencias</h3></div><br><br>";
		return new ModelAndView("home", "message", message);
	}
}